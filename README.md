# Merrick Starter-kit 

## Возможности:
- React
- Redux
- React router v4
- Formik
- Yup
- Webpack v4
- Styled-components with stylelint linting
- Automatic HTML generation
- ECMAScript 6 and JSX support
- Async Component
- ESLint
- Hot Module Replacement && React Hot Loader
- Storybook

### Соглашения
Для создания redux архитектуры используется [Ducks](https://github.com/erikras/ducks-modular-redux)
Для описания стилей подход CSS-in-JS [styled-components](https://www.styled-components.com/)
Для создания форм используется [formik](https://www.npmjs.com/package/formik)

Поставлен хук на precommit который запускает линтеры для js и css, если один из них возвращает ошибку, коммит не будет выполнен.

### Необходимые инструменты
- `eslint` - Для вашего редактора, для посвечивания js ошибок.
- `stylelint` - Для вашего редактора, для посвечивания css ошибок

### Правила создания веток
- Фича ветки имеют формат названия - `fb-номер-задачи`
- Фикс ветки имеют формат названия - `fix-номер-задачи`
