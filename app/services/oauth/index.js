export default {
  google: () => document.location.replace(`${document.location.origin}${__ENV__.googleUrl}`),
  github: () => document.location.replace(`${__ENV__.apiServer}${__ENV__.githubUrl}`)
};
