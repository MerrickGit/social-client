import http from '@/services/http';

const refresh = ({ refreshToken }) =>
  http.post('api/v1/users/refresh', { refreshToken }).then(res => res.data);

const login = preloginToken =>
  http
    .post('/api/v1/users/login', {
      preloginToken
    })
    .then(res => res.data);

const getUserInfo = () => http.get('/api/v1/users').then(res => res.data);

export default { login, getUserInfo, refresh };
