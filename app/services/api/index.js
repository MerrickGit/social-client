import v1 from './v1';

const apiConfig = { v1 };

export default apiConfig;
