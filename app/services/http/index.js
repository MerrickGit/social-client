import axios from 'axios';
import api from '@/services/api';

const http = axios.create({
  baseURL: __ENV__.apiServer
});

const updateToken = ({ cachedToken, accessToken, refreshToken }, onlyAxios) => {
  http.defaults.headers.common.Authorization = `Bearer ${accessToken || cachedToken}`;
  if (!onlyAxios) {
    localStorage.accessToken = accessToken;
    localStorage.refreshToken = refreshToken;
  }
};

http.interceptors.response.use(
  res => res,
  err => {
    const { refreshToken } = localStorage;
    if (err.response.status === 401 && refreshToken) {
      return api.v1.users
        .refresh({ refreshToken })
        .then(res => {
          /* eslint-disable-next-line */
          err.config.headers.Authorization = `Bearer ${res.data.accessToken}`;
          return res;
        })
        .then(res => updateToken(res.data))
        .then(() => http.request(err.config));
    }
    return Promise.reject(err);
  }
);

export { updateToken };
export default http;
