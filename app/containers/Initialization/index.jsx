import React from 'react';
import PropTypes from 'prop-types';
import api from '@/services/api';
import { withRouter } from 'react-router-dom';
import LoaderLayout from '@/layouts/Loader/index.jsx';
import { updateToken } from '@/services/http';
import { paths } from '@/routes/config';

class Initialization extends React.Component {
  state = {
    status: 'loading'
  };

  async componentDidMount() {
    const { history } = this.props;
    const searchParams = new URLSearchParams(document.location.search);
    const token = searchParams.get('token');
    const cachedToken = localStorage.accessToken;

    if (token) {
      window.history.replaceState({}, '', '/');
      const { data } = await api.v1.users.login(token);
      updateToken(data);
    } else if (cachedToken) {
      updateToken({ cachedToken }, true);
      const user = await api.v1.users.getUserInfo();
      console.log(user);
    } else {
      history.replace(paths.auth);
    }

    this.setState({ status: 'loaded' });
  }

  render() {
    const { children } = this.props;
    const { status } = this.state;

    switch (status) {
      case 'loading':
        return <LoaderLayout />;
      case 'loaded':
        return children;
      default:
        return null;
    }
  }
}

Initialization.propTypes = {
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]).isRequired
};

export default withRouter(Initialization);
