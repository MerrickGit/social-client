import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { GlobalNav, GlobalItem } from '@atlaskit/navigation-next';
import SignInIcon from '@atlaskit/icon/glyph/sign-in';
import AddIcon from '@atlaskit/icon/glyph/add';
import SettingsIcon from '@atlaskit/icon/glyph/settings';
import { DropdownItem, DropdownItemGroup, DropdownMenuStateless } from '@atlaskit/dropdown-menu';
import { paths } from '@/routes/config';

class GlobalItemWithDropdown extends React.Component {
  state = {
    isOpen: false
  };

  static propTypes = {
    trigger: PropTypes.func.isRequired,
    items: PropTypes.arrayOf(PropTypes.object).isRequired
  };

  handleOpenChange = ({ isOpen }) => this.setState({ isOpen });

  render() {
    const { items, trigger: Trigger } = this.props;
    const { isOpen } = this.state;
    return (
      <DropdownMenuStateless
        boundariesElement="window"
        isOpen={isOpen}
        onOpenChange={this.handleOpenChange}
        position="right top"
        trigger={<Trigger isOpen={isOpen} />}
      >
        {items}
      </DropdownMenuStateless>
    );
  }
}

const ItemComponent = ({ dropdownItems: DropdownItems, ...itemProps }) => {
  if (DropdownItems) {
    return (
      <GlobalItemWithDropdown
        trigger={({ isOpen }) => <GlobalItem isSelected={isOpen} {...itemProps} />}
        items={<DropdownItems />}
      />
    );
  }
  return <GlobalItem {...itemProps} />;
};

const Sidebar = ({ history }) => (
  <GlobalNav
    itemComponent={ItemComponent}
    primaryItems={[
      {
        dropdownItems: () => (
          <DropdownItemGroup title="What do you create?">
            <DropdownItem>Course</DropdownItem>
            <DropdownItem onClick={() => history.push(paths.createPost)}>Post</DropdownItem>
          </DropdownItemGroup>
        ),
        icon: AddIcon,
        id: 'actions',
        tooltip: 'Actions'
      }
    ]}
    secondaryItems={[
      {
        icon: SettingsIcon,
        id: 'settings',
        tooltip: 'Settings',
        onClick: () => console.log('Logo item clicked')
      },
      {
        icon: SignInIcon,
        id: 'login',
        tooltip: 'Login',
        onClick: () => history.push('/auth')
      }
    ]}
  />
);

Sidebar.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired
};

export default withRouter(Sidebar);
