import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import styled from 'styled-components';

import { updateCounter } from '@/models/mainReducer';

const Header = styled.header`
  display: flex;
  align-items: center;
`;

const Counter = ({ counter, updateCounter: update }) => (
  <Header>
    <p>
      Count:
      {counter}
    </p>
    <button type="button" onClick={() => update(counter + 1)}>
      Increment
    </button>
  </Header>
);

Counter.propTypes = {
  counter: PropTypes.number.isRequired,
  updateCounter: PropTypes.func.isRequired
};

const stateToProps = ({ main: { counter } }) => ({
  counter
});

const dispatchToProps = {
  updateCounter
};

const enhance = compose(
  connect(
    stateToProps,
    dispatchToProps
  )
);

export default enhance(Counter);
