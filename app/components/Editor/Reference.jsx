import React from 'react';
import Tag from '@atlaskit/tag';
import config from './config';

class Reference extends React.Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div>
        {Object.keys(config.editor.toggle).map(key => {
          const { shortcuts, desc } = config.editor.toggle[key];
          return (
            <div style={{ marginBottom: '15px' }}>
              <div>
                <Tag text={shortcuts} />
              </div>
              <div style={{ marginLeft: '5px', fontSize: '0.8em' }}>
                <span>{desc}</span>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Reference;
