import { getDefaultKeyBinding, KeyBindingUtil } from 'draft-js';
import config from './config';

const { isOptionKeyCommand } = KeyBindingUtil;

// Все команды делаются через комбинацию клавиш CMD + SHIFT + key
const myKeyBindingFn = e => {
  let globalCommand;

  Object.keys(config.editor.toggle).forEach(key => {
    const { keyCode, command } = config.editor.toggle[key];
    if (e.keyCode === keyCode && isOptionKeyCommand(e) && e.shiftKey) {
      globalCommand = command;
    }
  });

  if (!globalCommand) globalCommand = getDefaultKeyBinding(e);
  return globalCommand;
};

export default myKeyBindingFn;
