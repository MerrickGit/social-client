import { injectGlobal } from 'styled-components';

const editotStyles = () => injectGlobal`
  .DraftEditor-root {
    box-sizing: border-box;
    width: 100%;
    height: 100%;
    padding: 20px 15px 25px;

    & h1 {
      font-size: 6.1rem;
      letter-spacing: -.015em;
      line-height: 5.055555555555555rem;
      margin-top: 4.333333333333333rem;
      margin-bottom: 1.444444444444445rem;
      text-transform: uppercase;
      font-weight: bold;
      color: #222;

      &:after {
        content: ' ';
        display: block;
        width: 100px;
        background: #DB2B7B;
        margin-top: 1.444444444444445rem;
        margin-bottom: 1.444444444444445rem;
        height: 10px;
      }
    }

    & h2 {
      font-size: 2rem;
      color: #ccc;
      letter-spacing: 0.015rem;
      margin-top: 1.3rem;
      margin-bottom: 2.5rem;
    }

    & h3 {
      text-transform: uppercase;
      font-weight: 700;
      line-height: 1.877777777777778rem;
      margin-top: 2.888888888888889rem;
      margin-bottom: 1.444444444444445rem;
      font-size: 1.7rem;
      letter-spacing: -0.015rem;
    }

    & p {
      font-size: 1.4rem;
      max-width: 600px;
      line-height: 1.444444444444445rem;
      margin-bottom: 1.444444444444445rem;
    }

    & ul, ol {
      padding-left: 0;
      list-style-position: outside;
      list-style: disc;
    }

    .public-DraftStyleDefault-depth0.public-DraftStyleDefault-listLTR {
      margin-left: 1em;
    }

  }

  .public-DraftEditorPlaceholder-inner {
    font-size: 20px;
    letter-spacing: -0.02em;
    font-kerning: normal;
  }
`;

export default editotStyles;
