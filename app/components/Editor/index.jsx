import React from 'react';
import { Editor, EditorState, RichUtils, CompositeDecorator } from 'draft-js';
import { Map } from 'immutable';
import keyBinding from './keyBinding';
import editorStyles from './editorStyles';
import config from './config';
import Reference from './Reference.jsx';

const List = props => <ul>{props.children}</ul>;
const Pre = props => <pre>{props.children}</pre>;

const blockRenderMap = Map({
  [config.editor.toggle.h1.command]: {
    element: 'h1'
  },
  [config.editor.toggle.h2.command]: {
    element: 'h2'
  },
  [config.editor.toggle.h3.command]: {
    element: 'h3'
  },
  [config.editor.toggle.list.command]: {
    element: 'li',
    wrapper: <List />
  },
  [config.editor.toggle.paragraph.command]: {
    element: 'p'
  },
  [config.editor.toggle.code.command]: {
    element: 'code',
    wrapper: <Pre />
  }
});

const Link = props => {
  const entity = props.contentState.getEntity(props.entityKey);
  const data = entity.getData();
  console.log(props);

  return (
    <a href={data.url} title={data.url} className="ed-link">
      {props.decoratedText}
    </a>
  );
};

const findLinkEntities = (contentBlock, callback, contentState) => {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return entityKey !== null && contentState.getEntity(entityKey).getType() === 'LINK';
  }, callback);
};

const decorator = new CompositeDecorator([
  {
    strategy: findLinkEntities,
    component: Link
  }
]);

class CustomEditor extends React.Component {
  state = { editorState: EditorState.createEmpty(), isReadOnly: false };

  componentDidMount() {
    editorStyles();
  }

  onChange = editorState => {
    this.setState({ editorState });
  };

  doBold = e => {
    const { editorState } = this.state;
    e.preventDefault();
    this.onChange(RichUtils.toggleInlineStyle(editorState, 'BOLD'));
  };

  toggleBlockType = blockType => {
    const { editorState } = this.state;
    this.onChange(RichUtils.toggleBlockType(editorState, blockType));
  };

  setLink = () => {
    const urlValue = prompt('Введите ссылку', '');
    const { editorState } = this.state;
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity('LINK', 'SEGMENTED', {
      url: urlValue
    });
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity });
    this.setState({
      editorState: RichUtils.toggleLink(newEditorState, newEditorState.getSelection(), entityKey)
    });
  };

  // focus = () => this.refs.editor.focus();

  handleKeyCommand = command => {
    let isHandled = 'not-handled';

    Object.keys(config.editor.toggle).forEach(key => {
      const { type, command: localCommand } = config.editor.toggle[key];
      if (command === localCommand) {
        if (type === 'block') {
          this.toggleBlockType(localCommand);
        }
        // else if (type === 'inline') {}
        isHandled = 'handled';
      }
    });

    return isHandled;
  };

  render() {
    const { editorState, isReadOnly } = this.state;
    return (
      <div>
        <div
          style={{
            border: '1px solid #ccc',
            marginLeft: '100px',
            width: '800px',
            height: '500px',
            overflowY: 'scroll',
            display: 'flex'
          }}
        >
          <Editor
            readOnly={isReadOnly}
            editorState={editorState}
            onChange={this.onChange}
            handleKeyCommand={this.handleKeyCommand}
            keyBindingFn={keyBinding}
            blockRenderMap={blockRenderMap}
            placeholder=" You can write your awesome post"
          />
          <Reference />
        </div>
        <button type="button" onClick={this.doBold}>
          Bold
        </button>
        <button type="button" onClick={this.setLink}>
          Set url
        </button>
        <button type="button" onClick={() => this.toggleBlockType('header-one')}>
          Set header
        </button>
        <button type="button" onClick={() => this.toggleBlockType('header-two')}>
          Set header 2
        </button>
        <button type="button" onClick={() => this.toggleBlockType('header-three')}>
          Set header 2
        </button>
        <button type="button" onClick={() => this.toggleBlockType('li')}>
          Set list
        </button>
        <button type="button" onClick={() => this.toggleBlockType('unstyled')}>
          Set unstyled
        </button>
        <button type="button" onClick={() => this.setState({ isReadOnly: !isReadOnly })}>
          Toggle readonly
        </button>
      </div>
    );
  }
}

export default CustomEditor;
