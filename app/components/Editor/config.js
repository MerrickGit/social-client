export default {
  editor: {
    toggle: {
      h1: {
        command: 'header-one',
        keyCode: 49,
        shortcuts: 'OPTION|ALT + SHIFT + 1',
        desc: 'Create first level header',
        type: 'block'
      },
      h2: {
        command: 'header-two',
        keyCode: 50,
        shortcuts: 'OPTION|ALT + SHIFT + 2',
        desc: 'Create second level header',
        type: 'block'
      },
      h3: {
        command: 'header-three',
        keyCode: 51,
        shortcuts: 'OPTION|ALT + SHIFT + 3',
        desc: 'Create third level header',
        type: 'block'
      },
      list: {
        command: 'li',
        keyCode: 76,
        shortcuts: 'OPTION|ALT + SHIFT + L',
        desc: 'Create default list',
        type: 'block'
      },
      paragraph: {
        command: 'unstyled',
        keyCode: 75,
        shortcuts: 'OPTION|ALT + SHIFT + K',
        desc: 'Create paragraph',
        type: 'block'
      },
      code: {
        command: 'code-block',
        keyCode: 74,
        shortcuts: 'OPTION|ALT + SHIFT + J',
        desc: 'Create code block',
        type: 'block'
      }
      // bold: 'toogle-bold',
      // italic: 'toogle-italic'
    }
  }
};
