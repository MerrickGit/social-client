import { asyncComponent } from 'react-async-component';

const paths = {
  main: '/',
  auth: '/auth',
  oauth: '/oauth',
  error: '/error',
  createPost: '/post/create',
  viewPost: '/post/show',
  updatePost: '/post/edit'
};

const routerConfig = [
  {
    name: 'Main page',
    component: asyncComponent({
      resolve: () => import('./Main/index.jsx')
    }),
    path: paths.main,
    exact: true
  },
  {
    name: 'Auth page',
    component: asyncComponent({
      resolve: () => import('./Auth/index.jsx')
    }),
    path: paths.auth,
    exact: true
  },
  // {
  //   name: 'Error',
  //   component: asyncComponent({
  //     resolve: () => import('./Error/index.jsx'),
  //   }),
  //   path: paths.error
  // }
];

export { paths };
export default routerConfig;
