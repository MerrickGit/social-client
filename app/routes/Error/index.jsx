import React from 'react';
import styled, { withTheme } from 'styled-components';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import SecondaryLayout from '@/layouts/Secondary/index.jsx';
import EditorErrorIcon from '@atlaskit/icon/glyph/editor/error';
import Button from '@atlaskit/button';

const Header = styled.header`
  display: flex;
  align-items: center;
`;

const Body = styled.p`
  padding: 10px 70px 25px 58px;
  max-width: 500px;
  line-height: 22px;
  font-size: ${({ theme }) => theme.size.p};
`;

const Footer = styled.footer`
  display: flex;
  justify-content: flex-end;
`;

const Text = styled.span`
  font-size: ${({ theme }) => theme.size.h1};
  padding-left: 10px;
`;

const ErrorPage = ({ history, location: { state }, theme: { colors } }) => (
  <SecondaryLayout>
    <div>
      <Header>
        <EditorErrorIcon size="xlarge" primaryColor={colors.R400} />
        <Text>{state.title}</Text>
      </Header>
      <Body>{state.message}</Body>
      <Footer>
        <Button appearance="link" onClick={() => history.replace('/')}>
          Go to Home
        </Button>
      </Footer>
    </div>
  </SecondaryLayout>
);

ErrorPage.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.shape({
      title: PropTypes.string
    })
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  theme: PropTypes.shape({
    colors: PropTypes.object
  }).isRequired
};

const enhance = compose(
  withTheme,
  withRouter
);

export default enhance(ErrorPage);
