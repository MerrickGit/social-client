import React from 'react';
import styled from 'styled-components';
import Button from '@atlaskit/button';
import SecondaryLayout from '@/layouts/Secondary/index.jsx';
import GithubIcon from '@/components/Icons/github.jsx';
import GoogleIcon from '@/components/Icons/google.jsx';
import oauth from '@/services/oauth';

const Divider = styled.span`
  padding-top: 20px;
  padding-bottom: 20px;
  font-size: ${({ theme }) => theme.size.p};
  display: flex;
  justify-content: center;
  color: ${({ theme }) => theme.colors.N70};
`;

const AuthPage = () => (
  <SecondaryLayout>
    <div>
      <Button iconBefore={<GithubIcon />} shouldFitContainer onClick={oauth.github}>
        Log in with github
      </Button>
      <Divider>OR</Divider>
      <Button iconBefore={<GoogleIcon />} shouldFitContainer onClick={oauth.google}>
        Log in with google
      </Button>
    </div>
  </SecondaryLayout>
);

export default AuthPage;
