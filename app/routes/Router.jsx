import React from 'react';
import { Route } from 'react-router-dom';

import config from './config';
import Initialization from '@/containers/Initialization/index.jsx';

const Router = () => (
  <Initialization>
    {config.map(({ path, component: Component, name, exact }) => (
      <Route key={name} exact={exact} path={path} render={props => <Component {...props} />} />
    ))}
  </Initialization>
);

export default Router;
