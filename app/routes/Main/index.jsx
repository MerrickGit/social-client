import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link, withRouter } from 'react-router-dom';
import Counter from '@/components/Counter/index.jsx';
import api from '@/services/api';
import Sidebar from '@/components/Sidebar/index.jsx';
import Editor from '@/components/Editor/index.jsx';

const Wrapper = styled.main`
  display: flex;
`;

const Main = ({ history }) => (
  <Wrapper>
    <Sidebar />
    <div>
      <h1>React app</h1>
      <Link to="/auth">Layout</Link>
      <button
        type="button"
        onClick={() => history.push('/error', { title: 'Error', message: 'Auth error' })}
      >
        error-page
      </button>
      <button type="button" onClick={api.v1.users.getMe}>
        Get me
      </button>
      <Counter />
      <div>
        <Editor />
      </div>
    </div>
  </Wrapper>
);

Main.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired
};

export default withRouter(Main);
