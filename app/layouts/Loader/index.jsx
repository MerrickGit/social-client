import React from 'react';
import styled from 'styled-components';
import Spinner from '@atlaskit/spinner';

const Background = styled.div`
  min-height: 100vh;
  background: ${({ theme }) => theme.colors.N200};
  display: flex;
  align-items: center;
  justify-content: center;
`;

const LoaderLayout = () => (
  <Background>
    <Spinner size="large" />
  </Background>
);

export default LoaderLayout;
