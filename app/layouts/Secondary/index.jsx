import styled from 'styled-components';
import React from 'react';
import PropTypes from 'prop-types';

const Background = styled.div`
  min-height: 100vh;
  background: ${({ theme }) => theme.colors.B300};
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  background: ${({ theme }) => theme.colors.N0};
  box-shadow: ${({ theme }) => theme.zindex.B400};
  border-radius: ${({ theme }) => theme.radius.D400};
  padding: 40px;
  min-width: 250px;
`;

const SecondaryLayout = ({ children }) => (
  <Background>
    <Wrapper>{children}</Wrapper>
  </Background>
);

SecondaryLayout.propTypes = {
  children: PropTypes.element.isRequired
};

export default SecondaryLayout;
