import { combineReducers } from 'redux';

import main from './mainReducer.js';

const reducers = combineReducers({
  main
});

export default reducers;
