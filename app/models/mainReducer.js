import keyMirror from '@/utils/keyMirror';

const actionTypes = keyMirror(['uiCounterUpdate']);

const updateCounter = count => dispatch =>
  dispatch({
    type: actionTypes.uiCounterUpdate,
    payload: count
  });

const INITIAL_STATE = {
  counter: 0
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case actionTypes.uiCounterUpdate:
      return { ...state, counter: payload };
    default:
      return state;
  }
};

export { updateCounter };
