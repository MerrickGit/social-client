// функция которая переданный массив элементов
// конвертирует в объект
export default keys => {
  const constants = {};

  keys.forEach(el => {
    constants[el] = el;
  });

  return constants;
};
