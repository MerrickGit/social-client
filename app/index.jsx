import ReactDOM from 'react-dom';
import React from 'react';
import 'url-search-params';

import App from './App.jsx';

const node = document.querySelector('#root');

ReactDOM.render(<App />, node);
