import colors from './colors';
import zindex from './zindex';
import radius from './radius';
import size from './size';

export default {
  colors,
  zindex,
  radius,
  size
};
