import React from 'react';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { BrowserRouter } from 'react-router-dom';

import Router from '@/routes/Router.jsx';
import reducers from '@/models';
import store from './utils/createStore';
import theme from './theme';

const App = () => (
  <AppContainer key={Math.random()}>
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <BrowserRouter>
          <Router />
        </BrowserRouter>
      </Provider>
    </ThemeProvider>
  </AppContainer>
);

if (module.hot) {
  module.hot.accept('@/models', () => {
    store.replaceReducer(reducers);
  });
}

export default App;
