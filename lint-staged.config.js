module.exports = {
  'lint-staged': {
    '*.{js, jsx}': ['npx eslint --no-ignore --fix', 'npx stylelint', 'git add']
  }
};
