const { resolve } = require('path');

const webpackConfigPath = './webpack.config.js';

module.exports = {
  extends: ['airbnb', 'prettier'],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 9,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true
    }
  },
  rules: {
    'prettier/prettier': 'error',
    'import/extensions': ['.js', '.jsx']
  },
  env: {
    browser: true,
    es6: true,
    node: true
  },
  globals: {
    __DEV__: true,
    __ENV__: true
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: resolve(__dirname, webpackConfigPath)
      }
    }
  },
  plugins: ['react', 'prettier']
};
