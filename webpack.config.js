const { resolve } = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const env = require('./config/env');

const PRODICTION_ENV = process.env.NODE_ENV === 'production';
const alias = {
  '@': resolve(__dirname, './app'),
  '@static': resolve(__dirname, './static')
};

const entryFilePath = './app/index.jsx';
const outputFilePath = './dist/';
const htmlTemplateFilePath = './static/index.html';
const publicPath = PRODICTION_ENV ? '/static/' : '/';

const config = () => {
  const configObject = {
    mode: PRODICTION_ENV ? 'production' : 'development',

    entry: {
      common: '@babel/polyfill',
      app: entryFilePath
    },

    devtool: PRODICTION_ENV ? 'source-map' : 'inline-source-map',

    output: {
      path: resolve(__dirname, outputFilePath),
      filename: 'scripts/[name].js',
      publicPath
    },

    resolve: {
      alias,
      extensions: ['.js', '.json', '.jsx']
    },

    // context: path.resolve(__dirname, '../'),

    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: { loader: 'babel-loader' }
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: {
            loader: 'file-loader',
            options: {
              outputPath: 'images/'
            }
          }
        }
      ]
    },

    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': PRODICTION_ENV
          ? JSON.stringify('production')
          : JSON.stringify('development'),
        __DEV__: !PRODICTION_ENV,
        __ENV__: JSON.stringify(PRODICTION_ENV ? env.prod : env.dev)
      }),
      new HtmlWebpackPlugin({
        template: htmlTemplateFilePath,
        inject: 'body',
        publicPath,
        cache: false
      }),
      new CopyWebpackPlugin([
        {
          from: './static/styles',
          to: './styles'
        },
        {
          from: './node_modules/draft-js/dist/Draft.css',
          to: './styles'
        }
      ]),
      new CleanWebpackPlugin([outputFilePath])
    ],

    optimization: {
      namedChunks: true,
      splitChunks: {
        chunks: 'all'
      }
    },

    devServer: {
      contentBase: resolve(__dirname, './dist'),
      disableHostCheck: true,
      publicPath,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      historyApiFallback: true,
      host: '127.0.0.1',
      hot: true,
      port: 3000,
      open: true
    }
  };

  if (!PRODICTION_ENV) {
    configObject.plugins.push(new webpack.HotModuleReplacementPlugin());
  }

  return configObject;
};

module.exports = config;
