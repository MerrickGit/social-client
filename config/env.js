module.exports = {
  dev: {
    githubUrl: '/auth/oauth/github',
    apiServer: 'http://127.0.0.1',
    googleUrl: '/auth/oauth/google'
  },
  prod: {
    githubUrl: '/auth/oauth/github',
    apiServer: 'http://35.228.117.171',
    googleUrl: '/auth/oauth/google'
  }
};
